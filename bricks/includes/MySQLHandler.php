<?php
require_once(dirname(dirname(__FILE__)) . '/LocalSettings.php');

ini_set('display_errors', 1);
error_reporting(E_ALL);

// Create a MySQLi connection
$con = mysqli_connect($host, $dbuser, $dbpass);

// Check if the connection was successful
if (!$con) {
    die('Could not connect: ' . mysqli_connect_error());
}

// Select the database
mysqli_select_db($con, $dbname) or die("Unable to select database: $dbname");
?>
