# main.tf

provider "google" {
  credentials = file("<path-to-your-gcp-credentials-json>")
  project     = "your-gcp-project-id"
  region      = "us-central1" # Replace this with your desired GCP region
}

resource "google_compute_instance" "web_server" {
  name         = "web-server-instance"
  machine_type = "n1-standard-1" # Replace this with your desired machine type
  zone         = "us-central1-a" # Replace this with your desired GCP zone

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10" # Replace this with your desired OS image
    }
  }

  network_interface {
    network = "default"
  }

  tags = ["web-server"]

  metadata_startup_script = <<-EOT
    #!/bin/bash
    apt-get update
    apt-get install -y apache2 php libapache2-mod-php php-mysql
    cd /var/www/ && git clone ${gitrepo}
  EOT
}

resource "google_compute_firewall" "web_server_firewall" {
  name    = "web-server-firewall"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "22"]
  }

  source_tags = ["web-server"]
}

# Use the Docker provider to run MySQL and PHPMyAdmin as containers
provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "mysql_image" {
  name = "mysql:latest"
}

resource "docker_container" "mysql_container" {
  name  = "mysql_container"
  image = docker_image.mysql_image.latest
  env   = [
    "MYSQL_ROOT_PASSWORD=mysecretpassword",
    "MYSQL_DATABASE=mydb",
  ]
}

resource "docker_image" "phpmyadmin_image" {
  name = "phpmyadmin/phpmyadmin:latest"
}

resource "docker_container" "phpmyadmin_container" {
  name  = "phpmyadmin_container"
  image = docker_image.phpmyadmin_image.latest
  ports {
    internal = 80
    external = 8080
  }
  links = [docker_container.mysql_container.name]
  env   = [
    "PMA_HOST=${docker_container.mysql_container.name}",
    "PMA_PORT=3306",
  ]
}
