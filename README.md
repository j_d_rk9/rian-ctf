**Introduction**
Welcome to the Rian van Rijbroek-themed Capture The Flag (CTF) challenge! In this CTF, we will be using Terraform with a bricks theme to set up a virtual environment on Google Cloud Platform (GCP). Your mission is to solve various cybersecurity-related challenges inspired by the controversial cybersecurity expert Rian van Rijbroek.

**Prerequisites**
Before you begin, ensure you have the following:

A Google Cloud Platform (GCP) account.
Knowledge how to use terraform within GCP or how to use other cloudplatforms

**Setup Instructions GCP**
1. Clone this repository to your cloud console machine
3. terraform init, plan, apply

**Setup Instructions Vagrant**
1. Clone this repo
2. navigate to the repo
3. vagrant up in terminal 